
package ab;

public class arbol {
   na raiz;
   
   public arbol(){
      raiz=null;
   }
   
   //insertar un nodo al arbol
   public void insertarnodo (int x, String n){
       na nuevo=new na (x,n);
       if(raiz==null){
           raiz=nuevo;
       }else{
         na auxiliar=raiz;
         na padre;
         while(true){
             padre=auxiliar;
             if(x<auxiliar.dato){
                 auxiliar=auxiliar.hijoI;
                 if(auxiliar==null){
                    padre.hijoI=nuevo;
                    return;
                 }
             }else{
                 auxiliar=auxiliar.hijoD;
                 if(auxiliar==null){
                     padre.hijoD=nuevo;
                     return;
                 }
             }
         }          
       }
   }
   public boolean vacio(){
       return raiz==null;
   }
   
   public void inorden (na l){
       if(l!=null){
           inorden(l.hijoI);
           System.out.println(l.dato);
           inorden(l.hijoD);
       }
   }
   
   public void preorden (na l){
       if(l!=null){
           System.out.println(l.dato);
           preorden(l.hijoI);
           preorden(l.hijoD);
       }
   }
   
   public void posorden(na l){
       if(l!=null){
           posorden(l.hijoI);
           posorden(l.hijoD);
           System.out.println(l.dato);
       }
   }
   public boolean eliminarnodo(int d){
       na aux= raiz;
       na padre= raiz;
       boolean hi=true;
       
       while(aux.dato!=d){
           padre=aux;
           
           if(d<aux.dato){
               hi=true;
               aux=aux.hijoD;
           }else{
               hi=false;
               aux=aux.hijoD;
           }
           if(aux == null){
               return false;
           }
       }
       if(aux.hijoI == null && aux.hijoD == null){
           if(aux==raiz){
               raiz=null;
           }else if (hi){
               padre.hijoI=null;
           }else{
               padre.hijoD=null;
           }
       }
       else if(aux.hijoD==null){
           if(aux==raiz){
               raiz=aux.hijoI;
           }else if(hi){
               padre.hijoI=aux.hijoD;
           }else{
               padre.hijoD=aux.hijoI;
           }
       }else{
          na nr=obtenernodoshijos (aux);
          if(aux==raiz){
              raiz=nr;
          }else if (hi){
              padre.hijoI=nr;
          }else{
              padre.hijoD=nr;
          }
          nr.hijoI=aux.hijoI;
       }
       return true;
   }
   
   public na obtenernodoshijos (na nodoshijos){
       na nodopadre=nodoshijos;
       na temporal=nodoshijos;
       na auxiliar=nodoshijos.hijoD;
       
       while (auxiliar !=null){
           nodopadre=temporal;
           temporal= auxiliar;
           auxiliar=auxiliar.hijoI;
       }
       if(temporal!=nodoshijos.hijoD){
           nodopadre.hijoI=temporal.hijoD;
           temporal.hijoD=nodoshijos.hijoD;
       }
       System.out.println("Cambiando nodo"+temporal.toString());
       return temporal;
   }
   public na buscarnodo(int d){
       na aux=raiz;
       while(aux.dato!=d){
           if(d< aux.dato){
               aux=aux.hijoI;
           }
           else{
               aux=aux.hijoD;
           }
           if(aux==null){
               return null;
           }
       }
       return aux;
   }
}
