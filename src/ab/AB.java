
package ab;

public class AB {

    public static void main(String[] args) {
      int dato;
      String nombre;
      
      arbol ab=new arbol();
      
     ab.insertarnodo(18, "Corrin");
      ab.insertarnodo(19,"Monica");
      ab.insertarnodo(20,"Nicol");
      ab.insertarnodo(21,"Marshal");
      ab.insertarnodo(22,"Patri");
      ab.insertarnodo(23,"Rufino");
      ab.insertarnodo(27,"Felix");
      ab.insertarnodo(30,"Ziva");

    
    System.out.println("=====InOrden=======");
    if(!ab.vacio()){
        ab.inorden(ab.raiz);
    }
    System.out.println("=====PreOrden=======");
    if(!ab.vacio()){
        ab.preorden(ab.raiz);
    }
    System.out.println("======PosOrden======");
    if(!ab.vacio()){
        ab.posorden(ab.raiz);
    }
    System.out.println("======BUscar======");
    int vb;
    vb=40;
    if(ab.buscarnodo(vb)==null){
        System.out.println("No existe el nodo");
    }
    else{
        System.out.println("Nodo localizado");
        System.out.println();
        System.out.println(ab.buscarnodo(vb).toString());
    }
    System.out.println("======Elimina======");
    int ve;
    ve=10;
    ab.eliminarnodo(ve);
    ab.preorden(ab.raiz);
    
        }
    }
